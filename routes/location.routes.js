const express = require('express');
connect = require('mongoose');
const Location = require('../models/Location');

const router = express.Router();


router.get('/', async (req, res, next) => {
  const { viewAll } = req.query;
  try {
    let locations = [];
    if (viewAll === 'true') {
      locations = await Location.find().populate('researchers');
    } else {
      locations = await Location.find();
    }
    return res.status(200).json(locations);
  } catch (error) {
    return next(error);
  }
});

router.post('/create', async (req, res, next) => {
  try {
    const {tese, ano, reseachers=[]} = req.body;
    const location = {
      tese,
      ano, 
      reseachers
    };
    const newLocation = new Location(location);
    const createdLocation = await newLocation.save();
    return res.status(201).json(createdLocation);
  } catch (error) {
    next(error);
  }
});

router.post('/:locationId/add-researchers', async (req, res, next) => {
  try {
    const { locationId } = req.params;
    const { researchId } = req.body;
    const updatedLocation = await Location.findByIdAndUpdate(
      locationId,
      { $push: { researchers: researchId } },
      { new: true }
    );
    return res.status(200).json(updatedLocation);
  } catch (error) {
    return next(error);
  }
});

module.exports = router;
