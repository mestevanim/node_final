Ese es el primer proyecto de Node con Mongo DB. La idea es desarrollar datos de un grupo de pesquisadores de pós-graduación. Un grupo de periodistas brasileños de que hago parte.

En este proyecto:
1o) parto del framework Express para el desarrollo de backend y endpoints;
2o) Claudinary para la fotos em remoto
3o) Mongo Compass para base local - in localhost:4000;
4o) Mongo Atlas DB para base remoto;
5o) Postman para hacer los tests y cambios (create, delete, edición);
6o) Autenticación con Passport; 
7o) Vercel para desplegar el proyecto en remoto;
8o) El proyecto Node Final se encuentra en Git Lab; 

Descripción
1) Fueron 2 colecciones, Research y Location, con datos de los pesquisadores;
2) User para el registro, login e logout;
3) Las sesións de peticiones fueram guardadas en cookie;
4) Hay carpeta con controle de errores;
5) En la coleccion Research hay CRUD; 
6) Multer para subida de fichero;

