const mongoose = require('mongoose');

const Schema = mongoose.Schema;

// Creamos el esquema de pesquisadores
const researchSchema = new Schema(
  {
    name: { type: String, required: true },//La propiedad required hace que el campo sea obligatorio
    degree: { type: String, required: true },
    researchTitulacion: { type: String, required: true },
    picture: { type: String },
  },
  {
    // Esta propiedad servirá para guardar las fechas de creación y actualización de los documentos
    timestamps: true,
  }
);

// Creamos y exportamos el modelo Research
const Research = mongoose.model('Research', researchSchema);
module.exports = Research;

