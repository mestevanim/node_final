const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const locationSchema = new Schema(
  {
    tese: { type: String, required: true },
    ano: { type: Number },
		// Tipo mongoose Id y referencia al modelo Research
    researchers: [{ type: mongoose.Types.ObjectId, ref: 'Research' }],
  },
  {
    timestamps: true,
  }
);

const Location = mongoose.model('Location', locationSchema);
module.exports = Location;
